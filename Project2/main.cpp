#include <iostream>
#include <SDL2/SDL.h>
#include "display.h"
#include "mesh.h"
#include "shader.h"
#include "texture.h"
#include "camera.h"

static const int DISPLAY_WIDTH = 800;
static const int DISPLAY_HEIGHT = 600;

int main(int argc, char** argv)
{
	Display display(DISPLAY_WIDTH, DISPLAY_HEIGHT, "OpenGL");

	Vertex vertices[] =
	{
		Vertex(glm::vec3(-0.5, -0.5, 0), glm::vec2(0, 0), glm::vec3(0, 0, -1)),
		Vertex(glm::vec3(0, 0.5, 0), glm::vec2(0.5, 1.0), glm::vec3(0, 0, -1)),
		Vertex(glm::vec3(0.5, -0.5, 0), glm::vec2(1.0, 0.0), glm::vec3(0, 0, -1)) };

	unsigned int indices[] = { 0, 1, 2};

	Mesh mesh(vertices, sizeof(vertices) / sizeof(vertices[0]), indices, sizeof(indices) / sizeof(indices[0]));
	//Mesh crayon("C:\\Users\\mille\\Desktop\\maya\\blue_crayon.obj");
	Mesh crayonBox("C:\\Users\\mille\\Desktop\\maya\\colored_box.obj");
	Shader shader("C:\\Users\\mille\\Desktop\\maya\\basicShader");
	//Texture texture("C:\\Users\\mille\\Desktop\\maya\\blueCrayon.png");
	Texture texture2("C:\\Users\\mille\\Desktop\\crayonbox2.png");
	Transform transform;
	Camera camera(glm::vec3(0, 0, -24), 70.0f, (float)DISPLAY_WIDTH / (float)DISPLAY_HEIGHT, 0.1f, 1000.0f);

	SDL_Event e;
	bool isRunning = true;
	float counter = 0.0f;
	while (isRunning)
	{
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
				isRunning = false;
		}

		display.Clear(0.0f, 0.15f, 0.3f, 1.0f);

		float sinCounter = sinf(counter);
		float absSinCounter = abs(sinCounter);

		//transform.GetPos()->x = sinCounter;
		//transform.GetRot()->y = counter * 100;
		//transform.GetRot()->z = counter * 100;
		//transform.GetScale()->x = absSinCounter;
		//transform.GetScale()->y = absSinCounter;

		shader.Bind();
		//texture.Bind();
		shader.Update(transform, camera);
		//crayon.Draw();
		crayonBox.Draw();
		//mesh.Draw();

		display.SwapBuffers();
		SDL_Delay(1);
		counter += 0.01f;
	}

	return 0;
}