#pragma once

#include <string>
#include <GL/glew.h>
class Texture
{
public:
	Texture(const std::string& filename);

	void Bind(unsigned int uint);

	virtual ~Texture();
protected:
private:
	Texture(const Texture& other) {}
	void operator=(const Texture& other) {}

	GLuint m_texture;
};

